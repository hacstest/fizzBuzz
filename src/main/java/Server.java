import static spark.Spark.*;

public class Server {
        public static void main(String[] args) {
            FizzBuzz fb= new FizzBuzz();
            port(getHerokuAssignedPort());
            get("/hello/:name", (request, response) -> fb.generateFb(Integer.parseInt(request.params(":name"))));
        }

    static int getHerokuAssignedPort() {
        ProcessBuilder processBuilder = new ProcessBuilder();
        if (processBuilder.environment().get("PORT") != null) {
            return Integer.parseInt(processBuilder.environment().get("PORT"));
        }
        return 4567; //return default port if heroku-port isn't set (i.e. on localhost)
    }
}
